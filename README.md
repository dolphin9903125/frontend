# sistema-gerencial
Repositório referente ao sistema gerencial web

### Instruções
- Será necessário ter o node v18+ instalado em usa máquina e um gerenciador de pacotes como: npm, yarn ou pnpm
- Instale as dependências do projeto
- A aplicação usa o firebase, então será necessário colocar as configurações do projeto firebase no arquivo `.env`
  - Para isso basta renomear o arquivo `.env.example` para `.env` e colocar as credenciais
- Para subir a aplicação, rode npm dev