import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Professionals from '../views/Professionals.vue'
import HealthUnity from '../views/HealthUnity.vue'
import RegisterHealthUnity from '../views/RegisterHealthUnity.vue'
import RegisterProfessionals from '../views/RegisterProfessionals.vue'
import Schedule from '../views/Schedule.vue'
import RegisterAvailability from '../views/RegisterAvailability.vue'
import ScheduleAppointment from '../views/ScheduleAppointment.vue'
import Import from '../views/Import.vue'
import PagePlaceholder from '../views/PagePlaceholder.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: { breadcrumb: 'Home' }
    },
    {
      path: '/health-unities',
      name: 'healthUnity',
      component: HealthUnity,
      meta: { breadcrumb: 'Unidades de saúde' }
    }, {
      path: '/health-unities/register',
      name: 'registerHealthUnity',
      component: RegisterHealthUnity,
      meta: { breadcrumb: 'Cadastro' }
    },
    {
      path: '/professionals',
      name: 'professionals',
      component: Professionals
    },
    {
      path: '/professionals/register',
      name: 'registerProfessionals',
      component: RegisterProfessionals
    },
    {
      path: '/schedule',
      name: 'schedule',
      component: Schedule
    },
    {
      path: '/schedule/register-availability',
      name: 'registerAvailability',
      component: RegisterAvailability
    },
    {
      path: '/schedule-appointment',
      name: 'scheduleAppointment',
      component: ScheduleAppointment
    },
    {
      path: '/import',
      name: 'import',
      component: Import
    },
    {
      path: '/page-placeholder',
      name: 'pagePlaceholder',
      component: PagePlaceholder
    }
  ]
})

export default router
