import { initializeApp } from 'firebase/app';
import { getFirestore, collection } from 'firebase/firestore'
const env = (import.meta as any).env;
export const firebaseConfig = {
  apiKey: env.VITE_FIREBASE_API_KEY||'AIzaSyCuW8i9XoNG6J9RoKgcoteahra8_Gh2Xt8',
  authDomain: env.VITE_FIREBASE_AUTH_DOMAIN||'dolphin-project-4c8b4.firebaseapp.com',
  projectId: env.VITE_FIREBASE_PROJECT_ID||'dolphin-project-4c8b4',
  storageBucket: env.VITE_FIREBASE_STORAGE_BUCKET||'dolphin-project-4c8b4.appspot.com',
  messagingSenderId: env.VITE_FIREBASE_MESSAGING_SENDER_ID||'642659454744',
  appId: env.VITE_FIREBASE_APP_ID||'1:642659454744:web:fefe6e125bbe3b244c986d',
  measurementId: env.VITE_FIREBASE_MEASUREMENT_ID||'G-BMQVZP34MF',
};

export const firebaseApp = initializeApp(firebaseConfig);

export const FirestoreInstance = getFirestore(firebaseApp)
