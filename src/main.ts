import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
// Vuetify
import "@mdi/font/css/materialdesignicons.css";
import "@fortawesome/fontawesome-free/css/all.css";
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { fa } from "vuetify/iconsets/fa";
import { aliases, mdi } from "vuetify/lib/iconsets/mdi.mjs";
import { VTimePicker } from 'vuetify/labs/VTimePicker'
import DateFnsAdapter from '@date-io/date-fns'
import { VueFire } from 'vuefire'
import ptBR from 'date-fns/locale/pt-BR'
import App from './App.vue'
import router from './router'
import {firebaseApp} from './infra/firebase'

const vuetify = createVuetify({
  date: {
    adapter: DateFnsAdapter,
    locale: {
      en: ptBR,
    },
  },
  icons: {
    defaultSet: "mdi",
    aliases,
    sets: {
      mdi,
      fa
    },
  },
  components:{
    VTimePicker,
    ...components
  },
  directives,
})

const app = createApp(App)

app.use(createPinia())
app.use(vuetify)
app.use(router)
app.use(VueFire, {
  firebaseApp,
  modules: [
  ],
})

app.mount('#app')
